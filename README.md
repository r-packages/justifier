
<!-- README.md is generated from README.Rmd. Please edit that file -->

# <img src='man/figures/logo.png' align="right" style="height:250px" /> justifier 📦

## Human and Machine-Readable Justifications and Justified Decisions Based on ‘YAML’

<!-- badges: start -->

[![CRAN
status](https://www.r-pkg.org/badges/version/justifier)](https://cran.r-project.org/package=justifier)

[![Dependency
status](https://tinyverse.netlify.com/badge/justifier)](https://CRAN.R-project.org/package=justifier)

[![Pipeline
status](https://gitlab.com/r-packages/justifier/badges/main/pipeline.svg)](https://gitlab.com/r-packages/justifier/-/commits/main)

[![Downloads last
month](https://cranlogs.r-pkg.org/badges/last-month/justifier?color=brightgreen)](https://cran.r-project.org/package=justifier)

[![Total
downloads](https://cranlogs.r-pkg.org/badges/grand-total/justifier?color=brightgreen)](https://cran.r-project.org/package=justifier)

[![Coverage
status](https://codecov.io/gl/r-packages/justifier/branch/main/graph/badge.svg)](https://app.codecov.io/gl/r-packages/justifier?branch=main)

<!-- badges: end -->

The pkgdown website for this project is located at
<https://justifier.opens.science>. If the development version also has a
pkgdown website, that’s located at
<https://r-packages.gitlab.io/justifier>.

<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- Start of a custom bit for every package -->
<!-- - - - - - - - - - - - - - - - - - - - - -->

The goal of `justifier` is to provide a simple human- and
machine-readable standard for documenting justifications for decisions.
`justifier` was primarily developed to enable documenting the
development of behavior change interventions and the planning and
execution of scientific studies, but it can also be used to document
decisions in organisations, enabling deriving insights in decision
processes from the accumulation of decisions and types of justifications
over time.

Three vignettes (in progress) are located at:

- <https://justifier.opens.science/articles/general-introduction-to-justifier.html>
- <https://justifier.opens.science/articles/justifier-in-study-design.html>
- <https://justifier.opens.science/articles/justifier-in-intervention-development.html>

This meets the increasing demand for accountability of professionals
(see e.g. Van Woerkum & Aarts, 2012).

<!-- - - - - - - - - - - - - - - - - - - - - -->
<!--  End of a custom bit for every package  -->
<!-- - - - - - - - - - - - - - - - - - - - - -->

## Installation

You can install the released version of `justifier` from
[CRAN](https://CRAN.R-project.org) with:

``` r
install.packages('justifier');
```

You can install the development version of `justifier` from
[GitLab](https://about.gitlab.com) with:

``` r
remotes::install_gitlab('r-packages/justifier');
```

(assuming you have `remotes` installed; otherwise, install that first
using the `install.packages` function)

You can install the cutting edge development version (own risk, don’t
try this at home, etc) of `justifier` from
[GitLab](https://about.gitlab.com) with:

``` r
remotes::install_gitlab('r-packages/justifier@dev');
```

<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- Start of a custom bit for every package -->
<!-- - - - - - - - - - - - - - - - - - - - - -->

## References

van Woerkum, C. and Aarts, N. (2012), ‘Accountability: New challenges,
new forms’, *Journal of Organizational Transformation & Social Change*,
9, pp. 271–283, .

<!-- - - - - - - - - - - - - - - - - - - - - -->
<!--  End of a custom bit for every package  -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
